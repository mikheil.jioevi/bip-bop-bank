function goHome() {
  window.location.href = "../html/main.html";
}

document.addEventListener("DOMContentLoaded", function () {
  const translations = {
    en: {
      title: "About Bip-Bop Bank",
      mission: "Our Mission",
      missionContent:
        "At Bip-Bop Bank, our mission is to empower individuals, families, and businesses to achieve their financial goals...",
      setsApart: "What Sets Us Apart",
      approach: "Customer-Centric Approach",
      approachContent:
        "Your satisfaction is our priority. We are committed to delivering personalized service that goes beyond the transaction...",
      commitment: "Our Commitment to Security",
      commitmentContent:
        "Your trust is of utmost importance to us. We employ rigorous security measures to ensure the safety of your financial information...",
      joinUs: "Join Us on the Financial Journey",
      joinUsContent:
        "Whether you're saving for your first home, planning for retirement, or growing your business, Bip-Bop Bank is here to support you at every step...",
      footer:
        "Thank you for choosing Bip-Bop Bank. We look forward to being your preferred financial partner.",
    },
    geo: {
      title: "Bip-Bop Bank-ის შესახებ",
      mission: "Ჩვენი მისია",
      missionContent:
        "Bip-Bop Bank-ში ჩვენი მისიაა მივცეთ უფლება ინდივიდებს, ოჯახებს და ბიზნესს მიაღწიონ თავიანთ ფინანსურ მიზნებს...",
      setsApart: "რა გამოგვარჩევს",
      approach: "მომხმარებელზე ორიენტირებული მიდგომა",
      approachContent:
        "თქვენი კმაყოფილება ჩვენი პრიორიტეტია. ჩვენ მზად ვართ მოგაწოდოთ პერსონალიზებული სერვისი, რომელიც სცილდება ტრანზაქციას...",
      commitment: "ჩვენი ვალდებულება უსაფრთხოების მიმართ",
      commitmentContent:
        "თქვენი ნდობა ჩვენთვის უაღრესად მნიშვნელოვანია. ჩვენ ვიყენებთ უსაფრთხოების მკაცრ ზომებს თქვენი ფინანსური ინფორმაციის უსაფრთხოების უზრუნველსაყოფად...",
      joinUs: "შემოგვიერთდით ფინანსურ მოგზაურობაში",
      joinUsContent:
        "მიუხედავად იმისა, დაზოგავთ თქვენი პირველი სახლისთვის, გეგმავთ პენსიაზე გასვლას ან ავითარებთ თქვენს ბიზნესს, Bip-Bop Bank აქ არის, რათა დაგეხმაროთ ყოველ ნაბიჯზე..",
      footer:
        "გმადლობთ, რომ აირჩიეთ Bip-Bop Bank. ჩვენ მოუთმენლად ველით, რომ ვიყოთ თქვენი სასურველი ფინანსური პარტნიორი.",
    },
  };

  function updateTranslation() {
    const selectedLanguage = document.getElementById("language-selector").value;

    [
      "title",
      "mission",
      "sets-apart",
      "approach",
      "commitment",
      "join-us",
      "footer",
    ].forEach(function (elementId) {
      const translatedText = translations[selectedLanguage][elementId];
      document.getElementById(`translated-${elementId}`).textContent =
        translatedText;
    });
  }

  document
    .getElementById("language-selector")
    .addEventListener("change", updateTranslation);

  updateTranslation();
});
