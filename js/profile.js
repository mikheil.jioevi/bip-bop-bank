var person = [
  {
    username: "Mikheil",
    surname: "Jioevi",
    email: "Mikheil.Jioevi@gau.edu.ge",
    mobile: "+995-511-17-41-59",
    password: "Saswauli",
    id: "01234567891",
  },
];

const cards = [
  {
    id: 1,
    name: "credit 1",
    balance: "400",
    img: "card-1.png",
    expense: "445",
    sallary: "845",
  },
  {
    id: 2,
    name: "credit 2",
    balance: "500",
    img: "card-2.png",
    expense: "642",
    sallary: "1142",
  },
  {
    id: 3,
    name: "credit 3",
    balance: "600",
    img: "card-3.png",
    expense: "740",
    sallary: "1340",
  },
  {
    id: 4,
    name: "credit 4",
    balance: "150",
    img: "card-4.png",
    expense: "195",
    sallary: "345",
  },
];

var indexer = 0;

function nexCard() {
  if (indexer < cards.length - 1) {
    indexer++;
  } else {
    indexer = 0;
  }

  console.log(indexer);
  cardSlider();
}

function prevCard() {
  if (indexer > 0) {
    indexer--;
  } else {
    indexer = 2;
  }

  console.log(indexer);
  cardSlider();
}

function cardSlider() {
  let card = document.getElementsByClassName("card-img");
  let name = document.getElementById("card-name");
  let balance = document.getElementById("card-balance");
  let expense = document.getElementById("card-expense");
  let sallary = document.getElementById("card-sallary");

  card[0].style.backgroundImage = `url("../imgs/cards/${cards[indexer].img}")`;
  name.innerText = cards[indexer].name;
  balance.innerText = `Balance: ${cards[indexer].balance}`;
  expense.innerText = cards[indexer].expense;
  sallary.innerText = cards[indexer].sallary;
}

document.addEventListener("DOMContentLoaded", cardSlider());

function loadProfile() {
  let username = document.getElementById("username");
  let email = document.getElementById("email");
  let id = document.getElementById("personId");
  let userInput = document.getElementById("username-input");
  let surnameInput = document.getElementById("surname-input");
  let emailInput = document.getElementById("email-input");
  let mobileInput = document.getElementById("mobile-input");
  let passwordInput = document.getElementById("password-input");

  username.innerText = person[0].username;
  email.innerText = person[0].email;
  id.innerText = `ID: ${person[0].id}`;
  userInput.value = person[0].username;
  surnameInput.value = person[0].surname;
  emailInput.value = person[0].email;
  mobileInput.value = person[0].mobile;
  passwordInput.value = person[0].password;
}
document.addEventListener("DOMContentLoaded", loadProfile());

function updateUser() {
  let username = document.getElementById("username");
  let email = document.getElementById("email");
  let userInput = document.getElementById("username-input").value;
  let surnameInput = document.getElementById("surname-input").value;
  let emailInput = document.getElementById("email-input").value;
  let mobileInput = document.getElementById("mobile-input").value;
  let passwordInput = document.getElementById("password-input").value;

  // Check if any input is empty or contains only spaces
  if (
    !userInput.trim() ||
    !surnameInput.trim() ||
    !emailInput.trim() ||
    !mobileInput.trim() ||
    !passwordInput.trim()
  ) {
    console.log("Carielia");
  } else {
    if (emailInput.includes("@")) {
    }
  }
}
