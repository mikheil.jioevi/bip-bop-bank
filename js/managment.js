var cards = [
  {
    id: 1,
    name: "credit 1",
    balance: 400.13,
    img: "card-1.png",
    expense: "445",
    sallary: "845",
  },
  {
    id: 2,
    name: "credit 2",
    balance: 500.0,
    img: "card-2.png",
    expense: "642",
    sallary: "1142",
  },
  {
    id: 3,
    name: "credit 3",
    balance: 600.34,
    img: "card-3.png",
    expense: "740",
    sallary: "1340",
  },
  {
    id: 4,
    name: "credit 4",
    balance: 150.75,
    img: "card-4.png",
    expense: "195",
    sallary: "345",
  },
];
var firstCardIndexer = 0;
var SecondCardIndexer = 1;

function nextCard(boxNum) {
  if (boxNum === 1) {
    if (firstCardIndexer < cards.length - 1) {
      firstCardIndexer++;
    } else {
      firstCardIndexer = 0;
    }
  } else if (boxNum === 2) {
    if (SecondCardIndexer < cards.length - 1) {
      SecondCardIndexer++;
    } else {
      SecondCardIndexer = 0;
    }
  }

  console.log(`first: ${firstCardIndexer}, second: ${SecondCardIndexer}`);

  changeCards();
}

function prevCard(boxNum) {
  if (boxNum === 1) {
    if (firstCardIndexer > 0) {
      firstCardIndexer--;
    } else {
      firstCardIndexer = cards.length - 1;
    }
  } else if (boxNum === 2) {
    if (SecondCardIndexer > 0) {
      SecondCardIndexer--;
    } else {
      SecondCardIndexer = cards.length - 1;
    }
  }

  console.log(`first: ${firstCardIndexer}, second: ${SecondCardIndexer}`);

  changeCards();
}

function changeCards() {
  let cardImg = document.getElementsByClassName("card-img");
  let cardName = document.getElementsByClassName("card-name");
  let cardValue = document.getElementsByClassName("card-value");

  cardImg[0].style.backgroundImage = `url("../imgs/cards/${cards[firstCardIndexer].img}")`;
  cardImg[1].style.backgroundImage = `url("../imgs/cards/${cards[SecondCardIndexer].img}")`;
  cardName[0].innerText = cards[firstCardIndexer].name;
  cardName[1].innerText = cards[SecondCardIndexer].name;
  cardValue[0].innerText = cards[firstCardIndexer].balance;
  cardValue[1].innerText = cards[SecondCardIndexer].balance;
}

function transfer() {
  let from = document.getElementsByName("ammount-input")[0].value;

  let fromInt = parseFloat(from);

  console.log(typeof cards[firstCardIndexer].balance);
  console.log(typeof fromInt);

  if (fromInt <= cards[firstCardIndexer].balance) {
    cards[SecondCardIndexer].balance += fromInt;
    cards[firstCardIndexer].balance -= fromInt;
    changeCards();
  } else {
    alert("Tanxa agemateba baratze myof balances");
  }
}

document.addEventListener("DOMContentLoaded", changeCards());
