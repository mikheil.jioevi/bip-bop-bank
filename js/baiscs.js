function menuOpener() {
  let menu = document.getElementsByClassName("header-inner__lines");
  let smallMenu = document.getElementsByClassName("header-inner__nav");

  if (!menu[0].classList.contains("active")) {
    menu[0].classList.add("active");
    smallMenu[0].classList.add("small-menu");
    smallMenu[0].style.left = "0";
  } else {
    menu[0].classList.remove("active");
    smallMenu[0].classList.remove("small-menu");
  }
}
